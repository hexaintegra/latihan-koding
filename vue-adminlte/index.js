const Foo = { template: '<div class="routes">foo  {{ $route.params.id }}</div>' }
const Bar = { template: '<div class="routes">bar</div>' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
let comp = httpVueLoader('my-component.vue');
let blank = httpVueLoader('components/blank.vue');
let tablecomp = httpVueLoader('components/table.vue');
console.log(comp)
const routes = [
  { path: '/foo/:id', component: Foo },
  { path:'/test', component: comp},
  { path:'/blank', component: blank},
  { path:'/table', component: tablecomp},
  { path: '/bar', component: Bar }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes: routes,
  mode: 'history' // short for `routes: routes`
})
var isAuthenticated = 1;
router.beforeEach((to, from, next) => {
console.log(to, from)
  if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' })
  else next()
})
// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
  router
}).$mount('#app')
