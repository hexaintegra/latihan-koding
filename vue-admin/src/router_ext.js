const routes = [{
        path: '/about',
        name: 'About',
        component: () => import('@/views/About.vue'),
        meta: {
            layout: 'AppLayoutAbout'
        }
    },
    {
        path: '/contacts',
        name: 'Contacts',
        component: () => import('@/views/Contacts.vue'),
        meta: {
            layout: 'AppLayoutContacts'
        }
    },
    {
        path: '/test',
        name: 'Test',
        component: () => import('@/views/Home.vue')
    },
    // 1 ridwan

    {
        path: '/home1',
        name: 'Hadirku',
        component: () => import('@/latihan2/AttendanceLive.vue')
    },

    {
        path: '/bank1',
        name: 'bank',
        component: () => import('@/latihan2/Kas_Bank.vue')

    }

    // 2 arik
    {
    path: '/tableberita',
    name: 'TableBerita',
    component: () => import('@/latihan/TableBerita.vue'),
    meta: {
      layout: 'AppLayoutAdmin'
    }
  },
  {
    path: '/home2',
    name: 'Home Finance',
    component: () => import('@/latihan2/HomeFinance.vue'),
    /*meta: {
      layout: 'AppLayoutAdmin'*/
    },{
    path: '/dashcontract',
    name: 'Dashboard Contract',
    component: () => import('@/latihan2/DashboardContract.vue'),
    /*meta: {
      layout: 'AppLayoutAdmin'*/
    },


    // 3 rama
    {
        path: '/job',
        name: 'job',
        component: () => import('@/latihan2/job.vue'),
        meta: {
            layout: 'AppLayoutAdmin'
        }
    },


]

export default routes