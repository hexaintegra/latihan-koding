import Vue from 'vue'
import App from './App.vue'
import router from './router'
import AppLayout from '@/layouts/AppLayout'

//import $ from 'jquery';
//window.$ = window.jQuery = $;

//console.log(dt)
require ('@/libs/adminlte')
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'
import 'datatables.net-bs4/css/dataTables.bootstrap4.css';

Vue.component('AppLayout', AppLayout)

Vue.config.productionTip = false
//Vue.prototype.$apiUrl = 'http://localhost/junior/web/api';
window.$apiUrl = 'http://localhost/junior/web/api';

//Vue.prototype.jQuery = jQuery
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
