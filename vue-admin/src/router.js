import Vue from 'vue'
import VueRouter from 'vue-router'
import routesExt from './router_ext'

Vue.use(VueRouter)

const Login = {
    template: '<div>Login</div>'
}

let routes = [{
        path: '/',
        name: 'Home',
        component: () => import('@/views/Home.vue'),
        meta: {
            layout: 'AppLayoutHome'
        }
    },
    {
        path: '/user',
        name: 'Pengguna',
        component: () => import('@/views/User.vue'),
        meta: {
            layout: 'AppLayoutAdmin',
            requiresAuth:true
        }
    },
    {
        path: '/table',
        name: 'Table',
        component: () => import('@/views/Table.vue'),
        meta: {
            layout: 'AppLayoutAdmin'
        }
    },
    {
        path: '/berita1',
        name: 'Berita',
        component: () => import('@/latihan/Berita.vue'),
        meta: {
            layout: 'AppLayoutAdmin'
        }
    },
   


    /*{
    path: '/coba',
    name: 'Coba',
    component: () => import('@/views/Coba.vue'),
    meta: {
      layout: 'AppLayoutAdmin'
    }
  },*/
    {
        path: '/kontak3',
        name: 'Kontak3',
        component: () => import('@/latihan/Kontak3.vue'),
        meta: {
            layout: 'AppLayoutAdmin'
        }
    },
    {
        path: '/kontak2',
        name: 'Kontak',
        component: () => import('@/latihan/Kontak2.vue'),
        meta: {
            layout: 'AppLayoutAdmin'
        }
    },
    {
        path: '/tabletrans',
        name: 'Table Transient',
        component: () => import('@/views/Tabletransient.vue'),
        meta: {
            layout: 'AppLayoutAdmin'
        }
    },
    /* {
      path: '/table3',
      name: 'Table3',
      component: () => import('@/views/table3.vue'),
      meta: {
        layout: 'AppLayoutAdmin'
      }
    },
    {
      path: '/hitung',
      name: 'Hitung',
      component: () => import('@/views/Hitung.vue'),
      meta: {
        layout: 'AppLayoutAdmin'
      }
    },*/
    {
        path: '/form',
        name: 'Form',
        component: () => import('@/views/Form.vue'),
        meta: {
            layout: 'AppLayoutAdmin' //      requiresAuth: true
        }
    },
    {
        path: '/permit/:id',
        name: 'Permission',
        component: () => import('@/views/Permit.vue'),
        meta: {
            layout: 'AppLayoutAdmin' //      requiresAuth: true
        }
    },
    /* {
      path: '/hitung',
      name: 'Hitung',
      component: () => import('@/views/Hitung.vue'),
      meta: {
        layout: 'AppLayoutAdmin' //      requiresAuth: true
      }
    },*/
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/Login.vue')
    }
     
]

routes = routes.concat(routesExt)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        console.log('auth')
            console.log('record', to)
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!localStorage.APP_TOKEN) {
            next({ name: 'Login' })

        } else {
            // get permission info from server
            setTimeout(function(){
                to.params.roles = ["create", "read", "update", "delete"];
                //record.meta.roles = record.name+Date.now();{query:{roles:record.name+Date.now()}}
                next({params:to.params}) // does not require auth, make sure to always call next()!
            }, 100);
            
        }
    } else {
        next() // does not require auth, make sure to always call next()!
    }
})
export default router